const express = require("express");
const cors = require("cors");
const fs = require("fs");
const path = require("path");
const bodyParser = require("body-parser");
const isExistFolder = require("./middlewares/isExistFolder");

const router = require("./routes/routes");
const logger = require("./middlewares/logger");
const app = express();
app.use(isExistFolder);

const PORT = process.env.PORT ?? 8080;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cors());
app.use(logger);

app.use(router);

app.listen(PORT, () => {
  console.log(`Express Server is listening on localhost:${PORT} `);
});
