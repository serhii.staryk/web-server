const getContent = require('../utils/getContent');
const getUploadedDate = require('../utils/getUploadedDate');
const getExtension = require('../utils/getExtension');
const ifExists = require('../utils/ifExists');
const getArrOfFilesInDir = require('../utils/getArrOfFilesInDir');

const getFile = (req, res) => {
    try {
        if (!ifExists(getArrOfFilesInDir(), req.params.filename)) {
            return res.status(400).json({
                message: `No file with '${req.params.filename}' filename found`,
            });
        }
        res.status(200).json({
            message: 'Success',
            filename: req.params.filename,
            content: getContent(req.params.filename),
            extension: getExtension(req.params.filename),
            uploadedDate: getUploadedDate(req.params.filename).birthtime,
        });
    } catch (error) {
        res.status(500).json({
            message: 'Server error',
        });
    }
};

module.exports = getFile;