const getArrOfFilesInDir = require('../utils/getArrOfFilesInDir');

const getFiles = (req, res) => {
    try {
        if (getArrOfFilesInDir().length === 0) {
           return res.status(400).json({
                message: 'Client error',
            });
        }

        res.status(200).json({
            message: 'Success',
            files: getArrOfFilesInDir(),
        });
    } catch (error) {
       return res.status(500).json({
            message: 'Server error',
        });
    }
}

module.exports = getFiles;