const getExtension = require("../utils/getExtension");
const ifExists = require("../utils/ifExists");
const createFile = require("../utils/createFile");
const getArrOfFilesInDir = require("../utils/getArrOfFilesInDir");
const data = require("../data");
const postFile = (req, res) => {
  try {
    if (!("content" in req.body) || !("filename" in req.body)) {
      return res.status(400).json({
        message: "Please specify 'content' parameter",
      });
    }

    if (ifExists(data.extensions, getExtension(req.body.filename)) === false) {
      return res.status(400).json({
        message:
          "Application support log, txt, json, yaml, xml, js file extensions",
      });
    }

    if (ifExists(getArrOfFilesInDir(), req.body.filename)) {
      return res.status(400).json({
        message: "File exists",
      });
    }

    createFile(req.body.filename, req.body.content);

    res.status(200).json({
      message: "File created successfully",
    });
  } catch (error) {
    return res.status(500).json({ message: "Server error" });
  }
};

module.exports = postFile;
