const express = require('express');

const getFiles = require('../controllers/getFiles');
const getFile = require('../controllers/getFile');
const postFile = require('../controllers/postFile');

const router = express.Router();

router.post('/api/files', postFile);

router.get('/api/files', getFiles);
router.get('/api/files/:filename', getFile);

module.exports = router;
