const fs = require("fs");
const path = require("path");

const filePath = path.join(__dirname, "..", "files");

const isExistFolder = (req, res, next) => {
  if (!fs.existsSync(filePath)) {
    fs.mkdirSync(filePath);
  }
  next();
};

module.exports = isExistFolder;
