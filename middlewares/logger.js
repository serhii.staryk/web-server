const fs = require('fs');
const path = require('path');

const logger = (req, res, next) => {
    const date = new Date();
    const log = `${req.method} / ${req.protocol} / ${req.hostname} / ${req.originalUrl} / ${date} / ${JSON.stringify(req.body)} \n`
    fs.appendFile(path.join(__dirname, '..', 'logs.log'), log, err => {
        if (err) throw err;
    });
    next();
};

module.exports = logger;
