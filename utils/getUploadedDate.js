const fs = require('fs');
const path = require('path');

const directoryPath = path.join(__dirname, '..', 'files');

const getUploadedDate = fileName =>
    fs.statSync(path.join(directoryPath, fileName), err => {
        if (err) throw err;
    });

module.exports = getUploadedDate;
