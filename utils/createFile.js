const path = require('path');
const fs = require('fs');

const createFile = (fileName, fileContent) => {
    try {
        fs.writeFileSync(path.join('files', fileName), fileContent);
    } catch (err) {
        throw err;
    }
};

module.exports = createFile;
