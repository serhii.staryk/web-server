const getExtension = filename => filename.split('.').pop();

module.exports = getExtension;